package lucene;

public class Comment {
    String type;
    String id;
    String content;
    String dateTime;
    public Comment(String type, String id, String content, String dateTime){
        this.type = type;
        this.id = id;
        this.content = content;
        this.dateTime = dateTime;
    }
}
