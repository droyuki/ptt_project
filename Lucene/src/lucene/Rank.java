package lucene;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Rank {
    public static String logPath = "C:\\Users\\droyuki\\Desktop\\PTT\\Log.txt";
    public ArrayList<Article> rankingByComment(ArrayList<Article> commentRank) throws IOException {
        Comparator<Article> comparator = new Comparator<Article>() {
            @Override
            public int compare(Article o1, Article o2) {
                // TODO Auto-generated method stub
                int totalA = o1.push + o1.boo + o1.comment;
                int totalB = o2.push + o2.boo + o2.comment;
                return totalB - totalA;
            }
        };
        Collections.sort(commentRank, comparator);
//        for (int i = 0; i < 5; i++) {
//            Article thisA = (Article) commentRank.get(i);
////            System.out.println(thisA.title + "\r\n COMMENT: " + (thisA.push + thisA.boo + thisA.comment)
////                    + "\r\n---------------\r\n");
//        }
        return commentRank;
    }
    public ArrayList<Article> rankingByReply(ArrayList<Article> commentRank) throws IOException {
        Comparator<Article> comparator = new Comparator<Article>() {
            @Override
            public int compare(Article o1, Article o2) {
                // TODO Auto-generated method stub
                int totalA = o1.reply;
                int totalB = o2.reply;
                return totalB - totalA;
            }
        };
        Collections.sort(commentRank, comparator);
//        for (int i = 0; i < 5; i++) {
//            Article thisA = (Article) commentRank.get(i);
////            System.out.println(thisA.title + "\r\n COMMENT: " + (thisA.push + thisA.boo + thisA.comment)
////                    + "\r\n---------------\r\n");
//        }
        return commentRank;
    }
}




//public static void main(String[] args) throws IOException {
//FileInputStream fis = new FileInputStream(logPath);
//BufferedReader br = new BufferedReader(new InputStreamReader(fis,
//      "UTF-8"));
//
//int re = 0, push = 0, boo = 0, comment = 0;
//String thisLine = null;
//String title = "";
//boolean endFlag = false;
//while ((thisLine = br.readLine()) != null) {
//  if (thisLine.startsWith("系列文數量"))
//      re = Integer.parseInt(thisLine.split("系列文數量: ")[1]);
//
//  else if (thisLine.startsWith("推"))
//      push = Integer.parseInt(thisLine.split("推: ")[1]);
//
//  else if (thisLine.startsWith("噓"))
//      boo = Integer.parseInt(thisLine.split("噓: ")[1]);
//
//  else if (thisLine.startsWith("->"))
//      comment = Integer.parseInt(thisLine.split("->: ")[1]);
//  else if (thisLine.startsWith("總和"))
//      comment = Integer.parseInt(thisLine.split("總和: ")[1]);
//  else if (thisLine.startsWith("[") || thisLine.startsWith("R"))
//      title = thisLine;
//
//  if (thisLine.contains("====")) {
//      endFlag = true;
//      System.out.println("標題: " + title);
//      System.out.println("Re: " + re);
//      System.out.println("推: " + push);
//      System.out.println("噓: " + boo);
//      System.out.println("->: " + comment);
//      System.out.println("總和: " + (push + boo + comment));
//      System.out.println("--");
//      Article a = new Article(re, push, boo, comment, title);
//      commentRank.add(a);
//      System.out.println("SIZE " + commentRank.size());
//  }
//}
//br.close();