package lucene;

public class Article {
    int push;
    int boo;
    int comment;
    int reply;
    int commentId;
    String title;
    String author;
    String content;
    String dateTime;
    public Article(int reply,int push,int boo, int comment, String title){
        this.reply = reply;
        this.push = push;
        this.boo = boo;
        this.comment = comment;
        this.title = title;
    }
    public Article(int reply,int push,int boo, int comment, int commentId, String title, String author,String content, String dateTime){
        this.reply = reply;
        this.push = push;
        this.boo = boo;
        this.comment = comment;
        this.title = title;
        this.content = content;
        this.dateTime = dateTime;
        this.commentId = commentId;
        this.author = author;
    }
    public int commentSum(){
        int s = this.push + this.boo + this.comment;
        return s;
    }
    
    public void update(int push, int boo, int commentId,int comment){
        this.push = push;
        this.boo = boo;
        this.commentId = commentId;
        this.comment = comment;
    }
}
