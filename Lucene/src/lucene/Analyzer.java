package lucene;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class Analyzer {
    public static String path = "C:\\bbs_crawler\\BBS\\fetched\\Gossiping_7637_0\\";
    public static String logPath = "C:\\Users\\droyuki\\Desktop\\PTT\\Log.txt";
    public static String jiebaPath = "C:\\Users\\droyuki\\Desktop\\PTT\\ForJieba.txt";
    public static String rankPath = "C:\\Users\\droyuki\\Desktop\\PTT\\Rank.txt";
    private static ArrayList<String> titleList = new ArrayList<String>();
    public static ArrayList<Article> articleList = new ArrayList<Article>();

    public ArrayList<Article> analyzing() throws IOException {
        // public static void main(String[] args) throws IOException {
        File f = new File(path); // 讀取"00這個資料夾"，要記得將此資料夾放置同個java file裡面
        ArrayList<String> fileList = new ArrayList<String>(); // 宣告一的動態陣列為String的型態，目的用來儲存檔名
        if (f.isDirectory()) // 如果f讀到的是資料夾，就會執行
        {
            String[] s = f.list(); // 宣告一個list
            System.out.println("size : " + s.length); // 印出資料夾裡的檔案個數
            for (int i = 0; i < s.length; i++) {
                // System.out.println(s[i]);
                fileList.add(s[i]); // 將檔名一一存到fileList動態陣列裡面
            }
        }
        String thisTitle = null;
        for (String file : fileList) {
            FileInputStream fis = new FileInputStream(path + file);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis,
                    "UTF-8"));
            while ((thisTitle = br.readLine()) != null) {
                if (thisTitle.contains("Title:")) {
                    String title = thisTitle.split("Title:")[1]
                            .split("- 看板 Gossiping - 批踢踢實業坊")[0];
                    titleList.add(title);
                }
            }
            br.close();
        }

        for (String file : fileList) {
            ArrayList<Comment> commentList = new ArrayList<Comment>();
            ArrayList<String> commentId = new ArrayList<String>();
            // System.out.println(file);
            try {
                FileInputStream fis = new FileInputStream(path + file);
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        fis, "UTF-8"));
                BufferedWriter fw = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(logPath, true), "UTF-8"));
                String thisLine = null;
                int re = 0, push = 0, boo = 0, comment = 0, lineNum = 0;
                String title = null, dateTime = null, author = null;
                StringBuffer content = new StringBuffer();
                boolean commentTag = false, contentTag = false;
                // while ((thisLine = br.readLine()) != null) {
                while (br.ready()) {
                    thisLine = br.readLine();
                    // System.out.print("??"+thisLine+"!!!\n");
                    lineNum++;
                    if (thisLine.length() == 0)
                        continue;
                    if (thisLine
                            .contentEquals("本網站已依台灣網站內容分級規定處理。此區域為限制級，未滿十八歲者不得瀏覽。"))
                        break;
                    if (thisLine.contains("※ 文章網址:")) {
                        commentTag = true;
                    }
                    if (thisLine.contains("Title:")) {
                        title = thisLine.split("Title:")[1]
                                .split("- 看板 Gossiping - 批踢踢實業坊")[0];
                        re = replyNum(title);
                        // System.out.println(title + "(" + file + ")");
                        fw.append(title + "(" + file + ")" + "\r\n");
                        fw.flush();
                        contentTag = true;

                    }
                    if (lineNum == 4) {
                        author = thisLine.split("作者")[1].split("[(]")[0];
                        dateTime = thisLine.split("看板Gossiping標題")[1]
                                .split("時間")[1];
                        // System.out.println(author);
                        // System.out.println(dateTime);
                    }
                    if (contentTag = true) {
                        content.append(thisLine + "\r\n");
                    }
                    if (commentTag) {
                        String type, id, c, dt;
                        if (thisLine.startsWith("推")) {
                            type = "push";
                            String tmp = thisLine.substring(1,
                                    thisLine.length());
                            id = tmp.split(":")[0];
                            dt = thisLine.subSequence(thisLine.length() - 11,
                                    thisLine.length()).toString();
                            c = thisLine.split(id + ":")[1].split(dt)[0];
                            // c = thisLine.split("推")[1].split(":")[1].split();
                            Comment thisC = new Comment(type, id, c, dt);
                            commentList.add(thisC);
                            if (!commentId.contains(id))
                                commentId.add(id);
                            push++;
                        } else if (thisLine.startsWith("噓")) {
                            type = "boo";
                            String tmp = thisLine.substring(1,
                                    thisLine.length());
                            id = tmp.split(":")[0];
                            dt = thisLine.subSequence(thisLine.length() - 11,
                                    thisLine.length()).toString();
                            c = thisLine.split(id + ":")[1].split(dt)[0];
                            // c = thisLine.split("推")[1].split(":")[1].split();
                            Comment thisC = new Comment(type, id, c, dt);
                            commentList.add(thisC);
                            if (!commentId.contains(id))
                                commentId.add(id);
                            boo++;
                        } else if (thisLine.startsWith("→")) {
                            if (!thisLine.startsWith("※")) {
                                type = "comment";
                                String tmp = thisLine.substring(1,
                                        thisLine.length());
                                id = tmp.split(":")[0];
                                try {
                                    dt = thisLine.subSequence(
                                            thisLine.length() - 11,
                                            thisLine.length()).toString();

                                    c = thisLine.split(id + ":")[1].split(dt)[0];
                                    // c =
                                    // thisLine.split("推")[1].split(":")[1].split();
                                    Comment thisC = new Comment(type, id, c, dt);
                                    commentList.add(thisC);
                                    if (!commentId.contains(id))
                                        commentId.add(id);
                                    comment++;
                                } catch (Exception e) {
                                    System.out.println(e.getMessage());
                                    System.out.println(thisLine);
                                }
                            }
                        }
                    }
                }
                // ArrayList<Comment> tmpC = commentList;
                // for (int i = 0; i < commentList.size(); i++) {
                // Comment c = commentList.get(i);
                // for (int j = 0; j < commentList.size(); j++) {
                // Comment cc = commentList.get(j);
                // if (c.id == cc.id) {
                // tmpC.remove(j);
                // }
                // }
                // }
                fw.append("時間: " + dateTime + "\r\n");
                fw.append("Re : " + re + "\r\n");
                fw.append("推: " + push + "\r\n");
                fw.append("噓: " + boo + "\r\n");
                fw.append("->: " + comment + "\r\n");
                fw.append("回復數: " + (push + boo + comment) + "\r\n");
                fw.append("回復人次: " + commentId.size() + "\r\n");
                // fw.append("line: " + lineNum + "\r\n");
                fw.append("============= \r\n");
                fw.close();
                br.close();
                Article a = new Article(re, push, boo, comment,
                        commentId.size(), title, author, content.toString(),
                        dateTime);
                /* for 3/29 */
                BufferedWriter fww = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(jiebaPath, true), "UTF-8"));
                if (a.title.contains("急統")) { // test
                    articleList.add(a);
                    fww.append(a.content + "\r\n");
                } else
                    continue;
                fww.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Rank r = new Rank();
        // 刪除重複Re，合併計算推噓回復
        ArrayList<Article> aL = articleList;
        for (int i = 0; i < aL.size() - 1; i++) {
            for (int n = aL.size() - 1; n > i; n--) {
                if (aL.get(n).title.contains(aL.get(i).title)) {
                    Article tmp = aL.get(i);
                    int tmpPush = tmp.push;
                    int tmpBoo = tmp.boo;
                    int tmpComment = tmp.commentSum();
                    int tmpCommentId = tmp.commentId;
                    int tmpCommentIdN = aL.get(n).commentId;
                    int tmpCommentN = aL.get(n).comment;
                    int tmpPushN = aL.get(n).push;
                    int tmpBooN = aL.get(n).boo;
                    tmpPush += tmpPushN;
                    tmpBoo += tmpBooN;
                    tmpCommentId += tmpCommentIdN;
                    tmpComment += tmpCommentN;
                    aL.remove(n);
                    tmp.update(tmpPush, tmpBoo, tmpCommentId, tmpComment);
                    aL.set(i, tmp);
                }
            }
        }

        ArrayList<Article> result = r.rankingByReply(aL);
        BufferedWriter fw = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(rankPath, true), "UTF-8"));
        for (int i = 0; i < articleList.size(); i++) {
            Article aa = result.get(i);
            // System.out.println("標題: " + aa.title);
            // System.out.println("總回復: " + aa.reply);
            // System.out.println("--");
            fw.write("排名  " + (i + 1) + "\t");
            if (aa.title.startsWith("Re:")) {
                fw.write("標題: " + aa.title.split("Re:")[1] + "\t");
            } else {
                fw.write("標題: " + aa.title + "\t");
            }
            fw.write("作者: " + aa.author + "\t");
            fw.write("時間: " + aa.dateTime + "\t");
            fw.write("Re : " + aa.reply + "\t");
            fw.write("回應人次: " + aa.commentId + "\t");
            fw.write("回應次數: " + aa.commentSum() + "\t");
            fw.write("--\r\n");
        }
        fw.close();
        return result;
    }

    public static int replyNum(String title) {
        int result = 0;
        for (String compareTitle : titleList) {
            if (title.contains(compareTitle)) {
                result++;
            }
        }
        return result;
    }
}
