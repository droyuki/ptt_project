package json;
public class CommentNum{
    private int all;
    private int b;
    private int g;
    private int n;
    public int getAll() {
        return all;
    }
    public void setAll(int all) {
        this.all = all;
    }
    public int getB() {
        return b;
    }
    public void setB(int b) {
        this.b = b;
    }
    public int getG() {
        return g;
    }
    public void setG(int g) {
        this.g = g;
    }
    public int getN() {
        return n;
    }
    public void setN(int n) {
        this.n = n;
    }
}