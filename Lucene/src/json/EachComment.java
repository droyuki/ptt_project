package json;
public class EachComment{
    private String type;
    private String content;
    private String dateTime;
    private String commentAuthor;
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public String getDateTime() {
        return dateTime;
    }
    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
    public String getCommentAuthor() {
        return commentAuthor;
    }
    public void setCommentAuthor(String commentAuthor) {
        this.commentAuthor = commentAuthor;
    }
}