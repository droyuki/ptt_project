package json;
public class ArticleJson{
    private String id;
    private String author;
    private String title;
    private String dateTime;
    private String ip;
    private String content;
    private Comment comment;
    private CommentNum commentNum;
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getDateTime() {
        return dateTime;
    }
    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
    public String getIp() {
        return ip;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public Comment getComment() {
        return comment;
    }
    public void setComment(Comment comment) {
        this.comment = comment;
    }
    public CommentNum getCommentNum() {
        return commentNum;
    }
    public void setCommentNum(CommentNum commentNum) {
        this.commentNum = commentNum;
    }
    
}